# Autoincrement project version (NodeJS app)

Autoincrement project version with Gitlab CI. You can follow my guide [here](https://afivan.com/2022/02/10/autoincrement-project-version-with-gitlab-ci-easy-and-100-working/#primary)

## Getting started

The project index.js just outputs the version from the package.json file. The magic of incrementing the project versions and commit to the repository is done with Powershell scripts and Gitlab CI.

## Clone repository

```
git clone https://gitlab.com/afivan/node-autoincrement.git
```

## Installation
No installation of special modules is necessary. You can integrate the necessary files into your project very easily.

## Usage
Just run `npm start` and that's it!

## Support
For any questions, feedback or help please [contact me](https://afivan.com/contact/)

## Roadmap
There might be some new additions, stay tuned!

## Contributing
Any idea or contribution is more than welcomed!

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT license, everyone is free to share and contribute

## Project status
It will be more or less updated...
